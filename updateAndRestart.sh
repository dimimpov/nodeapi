#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo
sudo rm -rf nodeapi

# sudo apt-get install -y git && sudo apt-get install -y npm && sudo  apt-get install -y node

# clone the repo again
git clone https://gitlab.com/dimimpov/nodeapi.git

#source the nvm file. In an non
#If you are not using nvm, add the actual path like

sudo pm2 kill

sudo npm remove pm2 -g

sudo npm install pm2 -g

sudo pm2 status

#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.

cd nodeapi

#install npm packages
echo "Running npm install"
sudo npm install

#Restart the node server
sudo pm2 start server.js

exit