const config = require("config");

let plugins = [];

plugins.push({
  plugins: require("hapi-plugin-pg"),
  options: [
    {
      connectionString: `postgres: //${config.get("app.database").name}:${
        config.get("app.database").password
      }@${config.get("app.database").host}:${config.get("app.database").port}/${
        config.get("app.database").name
      }`
    }
  ]
});

module.exports = plugins;
