const Hapi = require("hapi");
const server = new Hapi.Server({ port: 443 });
// const plugins = require("./plugins");
const config = require("config");
const Sequelize = require("sequelize");

server.route({
  method: "GET",
  path: "/",
  handler: (request, h) => {
    const response = h.response({ message: "Hello world" });
    response.type("application/json");
    return response;
  }
});

const startServer = async function() {
  try {
    // add things here before the app starts, like database connection check etc
    await server.register({
      plugin: require("hapi-sequelizejs"),
      options: [
        {
          name: config.get("app.database").name, // identifier
          // ignoredModels: [__dirname + "/server/models/**/*.js"], // OPTIONAL: paths/globs to ignore files
          sequelize: new Sequelize(
            config.get("app.database").database,
            config.get("app.database").username,
            config.get("app.database").password,
            {
              dialect: config.get("app.database").dialect,
              host: config.get("app.database").host,
              port: config.get("app.database").port
            }
          ), // sequelize instance
          sync: false, // sync models - default false
          forceSync: false // force sync (drops tables) - default false
        }
      ]
    });
    // {
    //   register: require("hapi-plugin-pg"),
    //   options: {
    //     connectionString: `postgres: //${config.get("app.database").name}:${
    //       config.get("app.database").password
    //     }@${config.get("app.database").host}:${
    //       config.get("app.database").port
    //     }/${config.get("app.database").name}`
    //   }
    // },
    // err => {
    //   if (err) {
    //     throw err;
    //   }
    // }

    await server.start();
    console.log("server is running");
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};
const start = async () => {
  try {
    await startServer();
  } catch (error) {
    throw error;
  }
};

start();
module.exports = server;
